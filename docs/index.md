# FastAPI: JWT 및 종속성 주입을 통한 역할 기반 액세스 제어 <sup>[1](#footnote_1)</sup>

## 이 포스팅의 목표
다양한 권한 범위를 가진 엔드포인트를 노출하는 fastAPI 앱의 작동 예제를 제공한다.

## 목표 달성을 위한 단계
1. 종속성 주입 이해
1. JWT 인증
1. 보호된 경로의 역할과 권한

<a name="footnote_1">1</a>: 이 페이지는 [FastAPI: Role based access control with JWT and dependency injection](https://itnext.io/fastapi-jwt-authentication-with-scope-permissions-per-route-a42457fc85ca)를 편역한 것임.
