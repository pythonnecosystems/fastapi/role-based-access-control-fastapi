# FastAPI: JWT 및 종속성 주입을 통한 역할 기반 액세스 제어

## FastAPI에서 종속성 주입
종속성 주입은 코드의 느슨한 결합과 모듈화를 허용하는 강력한 소프트웨어 설계 패턴이다. 종속성 주입을 사용하면 종속성을 쉽게 관리하고 FastAPI 어플리케이션에 주입하여 유지 관리, 테스트 및 확장성을 높일 수 있다.

FastAPI의 종속성 주입 시스템을 활용하면 어플리케이션의 라우팅 함수에 종속성을 정의하고 해당 종속성의 인스턴스화와 주입을 FastAPI가 자동으로 처리하도록 할 수 있다.

FastAPI의 종속성 주입 기능을 사용하면 종속성 관리와 주입과 같은 번거로운 작업은 FastAPI가 처리하는 동안 깔끔하고 모듈화된 코드를 작성하는 데 집중할 수 있다. 이러한 접근 방식은 우수한 소프트웨어 엔지니어링 관행을 장려하고 FastAPI 애플리케이션의 전반적인 유지보수와 유연성을 향상시킨다.

주요 기능:

*(출처: FastAPI 문서)*

- 종속성에도 종속성이 있을 수 있으며, **종속성의 계층 구조 또는 "그래프"**를 생성할 수 있습니다.
- 이 모든 것은 프레임워크에서 **자동으로 처리**된다.
- 모든 종속성은 요청에서 데이터를 필요로 할 수 있으며 **경로 작업 제약 조건과 자동 문서화를 강화**할 수 있다.
- 종속성에 정의된 경로 작업 매개변수에 대해서도 **자동 유효성 검사**.
- 복잡한 사용자 인증 시스템, 데이터베이스 연결 등을 지원합니다.
데이터베이스, 프런트엔드 등과 타협하지 않습니다. 하지만 이 모든 것과 쉽게 통합할 수 있습니다.

FastAPI는 방대하고 상세한 설명서가 있으므로 여기서 몇 가지 예를 들어 다시 설명할 필요가 없습니다. 여기 링크를 따라 FastAPI에서 종속성 주입의 기본을 확인하고 실습할 수 있습니다.

## JWT 토큰을 사용하는 인증
[문서](https://fastapi.tiangolo.com/tutorial/security/simple-oauth2/)에 명시된 바와 같이 FastAPI는 OAuth2 보안 스키마로 보안을 즉시 지원할 수 있다. 이 메커니즘을 사용하면 간단한 사용자 이름/비밀번호 양식으로 인증하여 JWT 토큰을 얻을 수 있는 어플리케이션의 사용자를 만들 수 있다. 사용자가 이러한 토큰을 획득하면 이후 API 엔드포인트에 대한 요청에 대한 인증을 위해 베어러(Bearer) 토큰으로 사용할 수 있다.

OAuth2 스키마는 OAuth2PasswordBearer 클래스에 종속성을 주입하여 적용된다. 이 첫 번째 종속성은 요청자의 자격 증명이 데이터베이스 사용자의 항목과 일치한다고 가정할 때 요청자에게 JWT 토큰을 생성하고 반환하는 전용 엔드포인트에 주입할 수 있다. 이 첫 번째 종속성은 사용자가 자격 증명을 제공하는 여러 부분으로 구성된 양식에 의해 풀필먼트된다. 이 양식은 엔드포인트 문서 앞의 오른쪽 모서리에 자물쇠 아이콘이 있는 자동화된 FastAPI Swagger 문서에 나와 있다.

> **Note**:<br>
> OAuth2 스키마는 각 토큰에 대해 서로 다른 권한 수준을 활용하는 데 사용할 수 있는 범위 필드도 지원한다.
>
> 하지만 여기 예에서는 이 기능을 사용하지 않고 대신 사용자 모델에 모든 권한/범위 로직을 포함시키고 인증 중에 종속성을 삽입하겠다.

## 역할과 권한
모든 웹 어플리케이션에서 역할과 권한을 활용하면 적절한 역할과 권한을 가진 인증된 사용자에게만 중요한 리소스에 대한 액세스 권한을 부여할 수 있다. 이러한 세분화된 제어는 REST API의 보안을 강화하고, 사용자 데이터를 보호하며, 안전하고 확장 가능한 웹 어플리케이션을 구축하기 위한 견고한 기반을 제공한다.

사용 사례

1. 사용자 모델 - 사용자 엔티티를 생성, 읽기, 업데이트 및 삭제하는 모든 작업에 적용하기 위한 생성, 읽기, 업데이트 및 삭제 엔드포인트. 관리자만 이러한 리소스에 액세스할 수 있다.
1. 항목 모델 - 항목 엔티티를 읽기, 업데이트 및 삭제하는 모든 작업을 적용하기 위한 엔드포인트를 만들고, 읽고, 업데이트하고, 삭제합한다. 모든 사용자는 항목 엔티티를 만들고, 읽고, 업데이트할 수 있다. 또한 관리자는 항목을 삭제할 수도 있다.

## 데모
이제 이론은 제쳐두고 코드 예를 살펴보자.

전체 코드는 이 [Github 리포지토리](https://github.com/chrisK824/fastapi-permissioned-routes-example)에서 바로 사용할 수 있습니다.

자, 그럼 시작해 보자. 먼저 파일의 구조를 살펴보겠다.

```
➜  fastapi-permissioned-routes-example git:(main) tree                        
.
├── CODE_OF_CONDUCT.md
├── CONTRIBUTING.md
├── LICENSE
├── README.md
├── SECURITY.md
├── authentication.py
├── database.py
├── database_crud
│   ├── __init__.py
│   ├── items_db_crud.py
│   └── users_db_crud.py
├── db_models.py
├── local_storage.db
├── main.py
├── permissions
│   ├── base.py
│   ├── models_permissions.py
│   └── roles.py
├── requirements.txt
├── routers
│   ├── __init__.py
│   ├── items.py
│   └── users.py
└── schemas.py

4 directories, 21 files
```

**database.py**

```python
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, declarative_base

SQLALCHEMY_DATABASE_URL = "sqlite:///./local_storage.db"

engine = create_engine(SQLALCHEMY_DATABASE_URL,
                       connect_args={"check_same_thread": False})
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
```

sqlalchemy 라이브러리를 사용하여 local storage database session 객체를 생성하고 코드의 다른 곳에 종속성으로 삽입할 `get_db` 함수를 정의하여 데이터 조작 전에 데이터베이스와의 보안 연결을 만든다.

**db_models.py**

데이터베이스 테이블의 모델

```python
from sqlalchemy import Column, DateTime, Integer, String
from sqlalchemy.sql import func
from database import Base


class User(Base):
    __tablename__ = "users"
    email = Column(String, primary_key=True, index=True)
    password = Column(String)
    name = Column(String, nullable=True)
    surname = Column(String, nullable=True)
    role = Column(String)
    register_date = Column(DateTime, default=func.now())

    @property
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Item(Base):
    __tablename__ = "items"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
```

위 예의 `user` 모델을 위한 테이블과 간단한 `item`들을 저장하는 더미 테이블이다.

**schemas.py**

`user` 엔드포인트에 사용할 몇 가지 Pydantic 스키마이다.

```python
rom pydantic import BaseModel, EmailStr
from datetime import datetime
from typing import Optional

from permissions.roles import Role


class UserSignUp(BaseModel):
    email: EmailStr
    password: Optional[str]
    name: str
    surname: Optional[str] = None
    role: Role


class UserUpdate(BaseModel):
    name: Optional[str]
    surname: Optional[str]
    roles: Optional[Role]


class User(BaseModel):
    email: EmailStr
    name: str
    surname: Optional[str]
    role: Role
    register_date: Optional[datetime]

    class Config:
        orm_mode = True


class Token(BaseModel):
    access_token: str
    token_type: str


class ItemIn(BaseModel):
    name: str


class ItemUpdate(BaseModel):
    name: str


class Item(ItemIn):
    id: int

    class Config:
        orm_mode = True
```

**database_crud/users_db_crud.py**

`user` 모델 데이터베이스 테이블에 대한 CRUD 작업을 수행하는 데 필요한 모든 기능이다.

```python
from sqlalchemy.orm import Session
from db_models import User
import schemas as schemas
from sqlalchemy.exc import IntegrityError
from authentication import get_password_hash


class DuplicateError(Exception):
    pass


def add_user(db: Session, user: schemas.UserSignUp):
    user = User(
        email=user.email,
        password=get_password_hash(user.password),
        name=user.name,
        surname=user.surname,
        role=user.role
    )
    try:
        db.add(user)
        db.commit()
    except IntegrityError:
        db.rollback()
        raise DuplicateError(
            f"Email {user.email} is already attached to a registered user.")
    return user


def get_users(db: Session):
    users = list(db.query(User).all())
    return users


def update_user(db: Session, email: str, user_update: schemas.UserUpdate):
    user = db.query(User).filter(User.email == email).first()

    if not user:
        raise ValueError(
            f"There isn't any user with username {email}")

    updated_user = user_update.dict(exclude_unset=True)
    for key, value in updated_user.items():
        setattr(user, key, value)
    db.commit()
    return user


def delete_user(db: Session, email: str):
    user_cursor = db.query(User).filter(User.email == email)
    if not user_cursor.first():
        raise ValueError(f"There is no user with email {email}")
    else:
        user_cursor.delete()
        db.commit()
```

**database_crud/items_db_crud.py**

`item` 모델 데이터베이스 테이블에 대한 CRUD 작업을 수행하는 데 필요한 모든 함수이다.

```python
import schemas as schemas
from db_models import Item
from sqlalchemy.orm import Session


def create_item(db: Session, item: schemas.ItemIn):
    item = Item(
        name=item.name
    )
    db.add(item)
    db.commit()
    return item


def get_items(db: Session):
    items = list(db.query(Item).all())
    return items


def delete_item(db: Session, item_id: int):
    item_cursor = db.query(Item).filter(
        Item.id == item_id)
    if not item_cursor.first():
        raise ValueError(
            f"There is no item with ID {item_id}")
    else:
        item_cursor.delete()
        db.commit()


def update_item(db: Session, item_id: int, item_update: schemas.ItemUpdate):
    item = db.query(Item).filter(
        Item.id == item_id).first()

    if not item:
        raise ValueError(
            f"There isn't any item with ID {item_id}")

    updated_item = item_update.dict(exclude_unset=True)
    for key, value in updated_item.items():
        setattr(item, key, value)
    db.commit()
    return item
```

**routers/user.py**

`user` 모델과 관련된 엔드포인트를 라우팅한다. **`PermissionChecker` 호출 가능성에 대한 종속성은 잠시 후에 다시 설명하겠다**.

```python
import sys

sys.path.append("..")

from fastapi import Depends, APIRouter, HTTPException
from sqlalchemy.orm import Session
from fastapi.security import OAuth2PasswordRequestForm
from typing import List
from authentication import PermissionChecker, create_access_token, authenticate_user
from permissions.models_permissions import Users
from database import get_db
from database_crud import users_db_crud as db_crud
from schemas import User, UserSignUp, Token, UserUpdate



router = APIRouter(prefix="/v1")


@router.post("/users",
             dependencies=[Depends(PermissionChecker([Users.permissions.CREATE]))],
             response_model=User, summary="Register a user", tags=["Users"])
def create_user(user_signup: UserSignUp, db: Session = Depends(get_db)):
    """
    Registers a user.
    """
    try:
        user_created = db_crud.add_user(db, user_signup)
        return user_created
    except db_crud.DuplicateError as e:
        raise HTTPException(status_code=403, detail=f"{e}")
    except Exception as e:
        raise HTTPException(
            status_code=500, detail=f"An unexpected error occurred. Report this message to support: {e}")


@router.get("/users",
            dependencies=[Depends(PermissionChecker([Users.permissions.READ]))],
            response_model=List[User], summary="Get all users", tags=["Users"])
def get_users(db: Session = Depends(get_db)):
    """
    Returns all users.
    """
    try:
        users = db_crud.get_users(db)
        return users
    except Exception as e:
        raise HTTPException(
            status_code=500, detail=f"An unexpected error occurred. Report this message to support: {e}")


@router.patch("/users",
              dependencies=[Depends(PermissionChecker([Users.permissions.READ, Users.permissions.UPDATE]))],
              response_model=User,
              summary="Update a user", tags=["Users"])
def update_user(user_email: str, user_update: UserUpdate, db: Session = Depends(get_db)):
    """
    Updates a user.
    """
    try:
        user = db_crud.update_user(db, user_email, user_update)
        return user
    except ValueError as e:
        raise HTTPException(
            status_code=404, detail=f"{e}")
    except Exception as e:
        raise HTTPException(
            status_code=500, detail=f"An unexpected error occurred. Report this message to support: {e}")


@router.delete("/users",
               dependencies=[Depends(PermissionChecker([Users.permissions.DELETE]))],
               summary="Delete a user", tags=["Users"])
def delete_user(user_email: str, db: Session = Depends(get_db)):
    """
    Deletes a user.
    """
    try:
        db_crud.delete_user(db, user_email)
        return {"result": f"User with email {user_email} has been deleted successfully!"}
    except ValueError as e:
        raise HTTPException(
            status_code=404, detail=f"{e}")
    except Exception as e:
        raise HTTPException(
            status_code=500, detail=f"An unexpected error occurred. Report this message to support: {e}")


@router.post("/token", response_model=Token, summary="Authorize as a user", tags=["Users"])
def authorize(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    """
    Logs in a user.
    """
    print(form_data.username)
    print(form_data.password)
    user = authenticate_user(db=db, user_email=form_data.username, password=form_data.password)
    if not user:
        raise HTTPException(
            status_code=401, detail="Invalid user email or password.")
    try:
        access_token = create_access_token(data=user.email)
        return {
            "access_token": access_token,
            "token_type": "bearer"
        }
    except Exception as e:
        raise HTTPException(
            status_code=500, detail=f"An unexpected error occurred. Report this message to support: {e}")
```

**main.py**

실제 FastAPI 앱을 만들고 그 위에 경로 엔드포인트를 추가한다.

```python
import uvicorn
from fastapi import FastAPI
from contextlib import asynccontextmanager
from db_models import Base
from database import engine
from routers import users, items

description = """
Example API to demonstrate distinct permissioned routes
"""


@asynccontextmanager
async def lifespan(app: FastAPI):
    Base.metadata.create_all(bind=engine)
    yield


app = FastAPI(
    title='Permissioned routes example API',
    description=description,
    version="1.0.0",
    docs_url="/v1/documentation",
    redoc_url="/v1/redocs",
    lifespan=lifespan
)

app.include_router(users.router)
app.include_router(items.router)

if __name__ == '__main__':
    uvicorn.run(app, host="0.0.0.0", port=9999)
```

**authentication.py**

사용자 인증과 권한 부여에 사용되는 모든 유틸리티 함수.

```python
rom passlib.context import CryptContext
from datetime import datetime, timedelta
from jose import JWTError, jwt
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.orm import Session
from db_models import User
from database import get_db
from fastapi import Depends, HTTPException, status
from typing import List
from permissions.base import ModelPermission
from permissions.roles import get_role_permissions


class BearAuthException(Exception):
    pass


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

SECRET_KEY = "dummy_secret_key"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 60


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def create_access_token(data: str):
    to_encode = {"sub": data}
    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def get_token_payload(token: str = Depends(oauth2_scheme)):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        payload_sub: str = payload.get("sub")
        if payload_sub is None:
            raise BearAuthException("Token could not be validated")
        return payload_sub
    except JWTError:
        raise BearAuthException("Token could not be validated")


def authenticate_user(db: Session, user_email: str, password: str):
    user = db.query(User).filter(User.email == user_email).first()
    if not user:
        return False
    if not verify_password(password, user.password):
        return False
    return user


def get_current_user(db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    try:
        user_email = get_token_payload(token)
    except BearAuthException:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate bearer token",
            headers={"WWW-Authenticate": "Bearer"}
        )

    user = db.query(User).filter(User.email == user_email).first()
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Unauthorized, could not validate credentials.",
            headers={"WWW-Authenticate": "Bearer"}
        )
    return user


class PermissionChecker:
    def __init__(self, permissions_required: List[ModelPermission]):
        self.permissions_required = permissions_required

    def __call__(self, user: User = Depends(get_current_user)):
        for permission_required in self.permissions_required:
            if permission_required not in get_role_permissions(user.role):
                raise HTTPException(
                    status_code=status.HTTP_403_FORBIDDEN,
                    detail="Not enough permissions to access this resource")
        return user
```

1. 비밀번호를 해시와 해시 해제하는 함수 - 권장 사항에 따라 데이터베이스에 비밀번호가 해시된 값을 저장한다.
1. JWT 액세스 토큰을 생성하고 페이로드를 추출하는 함수 - 토큰의 페이로드는 사용자를 인증하는 데 필요한 정보이므로 이 경우 사용자 모델의 기본 키, 즉 이메일 주소가 된다.
1. `get_current_user` 함수는 토큰을 추출하고 이를 사용하여 이메일 페이로드를 데이터베이스에 있는 사용자의 기본 키와 일치시킨다. 토큰이 유효하지 않으면 예외를 발생한시킨다. 데이터베이스에서 사용자를 찾을 수 없으면 예외를 발시킨다. 이 함수는 주입된 종속성으로 `get_db` 함수와 `OAuth2PasswordBearer` 호출 가능 인스턴스를 사용하고 있다. 따라서 `get_current_user` 함수는 데이터베이스 저장소 사용을 위해 이미 생성되어 반환되는 연결과 `OAuth2` 체계에서 제공될 토큰에 의존한다. 이 함수는 엔드포인트에 대한 호출하는 사용자를 검색하는 데 사용되므로 종속성 자체가 종속성이므로 내부의 두 종속성, 즉 하위 종속성을 만든다.
1. 매직 메서드 `__call__`이 있는 `PermissionChecker` 클래스는 고급 사용자 정의 종속성 호출 가능 인젝션을 정의하는 방법입이다. `__call__` 메서드를 구현함으로써 클래스를 "호출 가능"하게 만들고, 이를 통해 FastAPI에서 종속성으로 사용할 수 있다. 이 메서드는 하위 종속성으로 `get_current_user`에 종속된다(이 하위 종속성은 위에서 설명한 자체 하위 종속성을 먼저 해결한다). 잠시 후 해당 코드로 돌아가겠다.

**permission/base.py**

```python
from enum import Enum
from typing import Union, Type
import re
from dataclasses import dataclass


@dataclass(init=False, eq=False)
class Permission:
    """
    Base class for permissions.
    Allows for easy comparison of different notations.
    E.g. Permission('CREATE') == 'CREATE'.
    """
    permission_type: str

    def __init__(self, permission_type: str):
        self.permission_type = str(permission_type)

    @property
    def full_name(self) -> str:
        return str(self.permission_type)

    def __str__(self):
        return self.full_name

    def __eq__(self, other):
        if (
            isinstance(other, str)
            or isinstance(other, Permission)
            or issubclass(other, Permission)
        ):
            return str(self.full_name) == str(other)
        return False

    def __hash__(self):
        return hash(self.full_name)


class PermissionType(str, Enum):
    """
    Enum for the different types of default
    permissions that can be applied to a model.
    """
    CREATE = "CREATE"
    READ = "READ"
    UPDATE = "UPDATE"
    DELETE = "DELETE"

    def __str__(self):
        return f"{self.value}"


@dataclass(eq=False)
class ModelPermission(Permission):
    """
    A higher-level of abstraction for the Permission class.
    A ModelPermission with permission_type=CREATE and permission_model=Example
    is equivalent to a Permission with permission_type=`Example_CREATE`.
    """
    permission_type: Union[PermissionType, str]
    permission_model: Type

    @property
    def full_name(self) -> str:
        model_name = re.sub(
            r"(?<!^)(?=[A-Z])", "_", self.permission_model.__name__
        ).upper()
        return f"{model_name}_{self.permission_type.__str__().upper()}"

    def __str__(self):
        return self.full_name


class ModelDefaultPermissions:
    """
    Class that provides a set of default permissions used by a model.
    It is used by the ModelPermissions class.
    """

    def __init__(self, model):
        self.CREATE = ModelPermission(
            permission_type=PermissionType.CREATE, permission_model=model
        )
        self.READ = ModelPermission(
            permission_type=PermissionType.READ, permission_model=model
        )
        self.UPDATE = ModelPermission(
            permission_type=PermissionType.UPDATE, permission_model=model
        )
        self.DELETE = ModelPermission(
            permission_type=PermissionType.DELETE, permission_model=model
        )


class ModelPermissions:
    """
    Provides the default set of permissions
    under the `permissions` attribute.
    """

    @classmethod
    @property
    def permissions(cls) -> ModelDefaultPermissions: # noqa
        return ModelDefaultPermissions(cls)
```

일련의 클래스를 통해 기본 모델 권한 클래스가 정의되며, 여기에는 모든 기본 유형의 권한이 속성으로 포함된다. 이 사용 사례에는 4가지 유형의 권한이 있다.

1. CREATE
1. READ
1. UPDATE
1. DELETE

`ModelPermissions` 클래스에는 기본 모델 권한 클래스의 인스턴스화만 반환하는 프로퍼티가 포함되어 있다.

**models_permission.py**

```python
from permissions.base import  ModelPermissions


class Users(ModelPermissions):
    pass

class Items(ModelPermissions):
    pass
```

데이터베이스와 엔드포인트 그룹화된 리소스 각각에 대해 래퍼 클래스를 만든다. 이 클래스는 `ModelPermissions` 클래스를 상속하므로 상속된 클래스의 권한 속성에 따라 이름을 기반으로 생성되는 기본 권한 그룹을 상속받는다.

따라서 모델, 사용자 및 항목은 다음과 같은 권한 객체를 파생할 수 있다.

1. Users_CREATE, Users_READ, Users_UPDATE, Users_DELETE
1. Items_CREATE, Items_READ, Items_UPDATE, Items_DELETE


**roles.py**

```python
from enum import Enum
from permissions.models_permissions import *


class Role(str, Enum):
    ADMINISTRATOR = "ADMINISTRATOR"
    USER = "USER"

    @classmethod
    def get_roles(cls):
        values = []
        for member in cls:
            values.append(f"{member.value}")
        return values


ROLE_PERMISSIONS = {
    Role.ADMINISTRATOR: [
        [
            Users.permissions.CREATE,
            Users.permissions.READ,
            Users.permissions.UPDATE,
            Users.permissions.DELETE
        ],
        [
            Items.permissions.CREATE,
            Items.permissions.READ,
            Items.permissions.UPDATE,
            Items.permissions.DELETE
        ]
    ],
    Role.USER: [
        [
            Items.permissions.CREATE,
            Items.permissions.READ,
            Items.permissions.UPDATE
        ]
    ]
}


def get_role_permissions(role: Role):
    permissions = set()
    for permissions_group in ROLE_PERMISSIONS[role]:
        for permission in permissions_group:
            permissions.add(str(permission))
    return list(permissions)
```

마지막으로 어플리케이션에 대한 두 가지 역할, 즉 관리자 역할과 단순 사용자 역할을 정의한다. 각 역할에는 권한 그룹이 할당되었다. 사용자 역할은 사용자 모델 리소스에 대한 액세스 권한이 없으며 항목 삭제 리소스에 대한 액세스 권한도 없다는 것을 알 수 있다.

## 권한 종속성 주입
이미 언급했듯이 `PermissionChecker` 클래스가 여기서 트릭을 수행한다. 종속성이 풀필먼트되면 데이터베이스에서 사용자 객체가 반환되거나 예외가 발생하여 호출자에게 리소스가 거부된다. 방법을 살펴보자.

1. 콜러블 자체는 인수를 받는데, 이 인수는 사용 사례에서 권한 객체이다.
1. 데이터베이스에서 사용자가 검색되었으므로 이제 어플리케이션의 에이전트로서의 역할을 지정하는 역할 필드에 액세스할 수 있다.
1. 호출자의 역할을 알았다는 것은 해당 역할에 할당된 권한을 알 수 있다는 뜻이다. 수신된 권한 중 경로를 '보호'하는 권한이 사용자 역할의 권한에 있으면 해당 사용자에게 리소스를 검색할 권한이 부여되고, 그렇지 않으면 예외가 발생하여 관련 메시지가 호출자에게 전달된다.

## 실제로 확인하기
직접 시도해 보려면 리포지토리의 `readme.md` 파일에 있는 단계를 따르는 것이 좋다.

- [리포지토리](https://github.com/chrisK824/fastapi-permissioned-routes-example)를 Git으로 복제/포크하여 컴퓨터에 저장한다. Python3와 Sqlite3 브라우저가 설치되어 있는지 확인한다.
- readme.md 지침에 따라 종속 요소를 설치하고 앱을 실행한다.

다음 명령은 모두 FastAPI에 의해 자동으로 파생되는 Swagger UI 문서를 통해 실행할 수 있지만, 더 나은 가시성을 위해 여기서는 `curl` 유틸리티를 사용하겠으며, 이는 정확히 동일할 뿐만 아니라 Swagger UI에서도 `curl` 명령이 자동으로 파생된다.

> 기본 관리자 사용자를 사용하여 자동화된 Swagger UI(localhost:9999/v1/documentation)에서 인증하고 몇 가지 엔드포인트를 사용해 보도록 한다.

- 먼저 ADMINISTRATOR 역할이 있는 사용자와 마찬가지로 로그인하여 `/v1/token` 엔드포인트를 사용하여 베어러 토큰을 검색한다.

```bash
➜  fastapi-permissioned-routes-example git:(main) 
$ curl -i -X 'POST' \
  'http://localhost:9999/v1/token' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'username=admin%40example.com&password=1234'
HTTP/1.1 200 OK
date: Thu, 22 Jun 2023 18:23:38 GMT
server: uvicorn
content-length: 181
content-type: application/json

{
    "access_token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhZG1pbkBleGFtcGxlLmNvbSIsImV4cCI6MTY4NzQ2MTgxOH0.AT4l4lcAfJzrcwetIiT9REmjN2uTOtGQzXxI_usitGs",
    "token_type":"bearer"
}
```

- ADMINISTRATOR 역할의 사용자로 로그인하면 다른 사용자를 만든다.

```bash
➜  fastapi-permissioned-routes-example git:(main) ✗ 
$ curl -i -X 'POST' \
  'http://localhost:9999/v1/users' \
  -H 'accept: application/json' \
  -d '{"email":"user@example.com", "password":1234,"name":"user name","surname":"user surname","role":"USER"}' \
  -H 'Content-Type: application/json' \
  -H  'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhZG1pbkBleGFtcGxlLmNvbSIsImV4cCI6MTY4NzQ2MTgxOH0.AT4l4lcAfJzrcwetIiT9REmjN2uTOtGQzXxI_usitGs'

HTTP/1.1 200 OK
date: Thu, 22 Jun 2023 18:39:14 GMT
server: uvicorn
content-length: 124
content-type: application/json

{
    "email":"user@example.com",
    "name":"user name",
    "surname":"user surname",
    "role":"USER",
    "register_date":"2023-06-22T18:39:15"
}
```

- ADMINISTRATOR 역할의 사용자로 로그인하여 모든 사용자를 가져온다.

```python
➜  fastapi-permissioned-routes-example git:(main) 
$ curl -i -X 'GET' \
  'http://localhost:9999/v1/users' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -H  'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhZG1pbkBleGFtcGxlLmNvbSIsImV4cCI6MTY4NzQ2MTgxOH0.AT4l4lcAfJzrcwetIiT9REmjN2uTOtGQzXxI_usitGs'
HTTP/1.1 200 OK
date: Thu, 22 Jun 2023 18:26:36 GMT
server: uvicorn
content-length: 263
content-type: application/json

[
    {
        "email":"admin@example.com",
        "name":"Admin name",
        "surname":"Admin surname",
        "role":"ADMINISTRATOR",
        "register_date":"2023-06-21T00:00:00"
    },
    {
        "email":"user@example.com",
        "name":"user name",
        "surname":"user surname",
        "role":"USER",
        "register_date":"2023-06-22T18:39:15"
    }
]
```

- ADMINISTRATOR 역할의 사용자로 로그인하면 item을 생성한다.

```python
➜  fastapi-permissioned-routes-example git:(main) ✗ 
$ curl -i -X 'POST' \
  'http://localhost:9999/v1/items' \
  -H 'accept: application/json' \
  -d '{"name":"item one"}' \
  -H 'Content-Type: application/json' \
  -H  'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhZG1pbkBleGFtcGxlLmNvbSIsImV4cCI6MTY4NzQ2MTgxOH0.AT4l4lcAfJzrcwetIiT9REmjN2uTOtGQzXxI_usitGs'

HTTP/1.1 200 OK
date: Thu, 22 Jun 2023 18:41:10 GMT
server: uvicorn
content-length: 26
content-type: application/json

{
    "name":"item one",
    "id":1
}
```

> 자, 이제 재미있는 부분을 살펴보겠다! 새 사용자가 무엇을 할 수 있는지 살펴보자.

- 먼저 USER 역할의 사용자와 마찬가지로 로그인하여 `/v1/token` 엔드포인트를 사용하여 베어러 토큰을 검색한다.

```bsh
➜  fastapi-permissioned-routes-example git:(main) ✗   
$ curl -i -X 'POST' \
    'http://localhost:9999/v1/token' \
    -H 'accept: application/json' \
    -H 'Content-Type: application/x-www-form-urlencoded' \
    -d 'username=user%40example.com&password=1234'
HTTP/1.1 200 OK
date: Thu, 22 Jun 2023 18:43:11 GMT
server: uvicorn
content-length: 180
content-type: application/json

{
    "access_token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1c2VyQGV4YW1wbGUuY29tIiwiZXhwIjoxNjg3NDYyOTkyfQ.vjZL8kYk1wNk-w-bZ4Lzum6tuMtBI9-V6wpqKlIgwJs",
    "token_type":"bearer"
}
```

- USER 역할의 사용자로 로그인하여 사용자 목록을 읽는다.

```python
➜  fastapi-permissioned-routes-example git:(main) ✗ 
$ curl -i -X 'GET' \
  'http://localhost:9999/v1/users' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -H  'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1c2VyQGV4YW1wbGUuY29tIiwiZXhwIjoxNjg3NDYyOTkyfQ.vjZL8kYk1wNk-w-bZ4Lzum6tuMtBI9-V6wpqKlIgwJs'
HTTP/1.1 403 Forbidden
date: Thu, 22 Jun 2023 18:44:17 GMT
server: uvicorn
content-length: 59
content-type: application/json

{
    "detail":"Not enough permissions to access this resource"
}
```

**예상대로 USER 역할을 가진 새 사용자는 `/v1/users` 엔드포인트에 액세스할 수 없다. 이 엔드포인트는 USER_READ 권한으로 보호되고 USER 역할은 해당 권한 그룹에 이 권한이 할당되어 있지 않으므로 관련 `403 Forbidden` http 코드가 무슨 일이 있었는지 설명하는 메시지와 함께 반환된다!**

멋지지 않나요?! 😎

- USER 역할의 사용자로 로그인하면 항목 목록을 볼 수 있다.

```python
➜  fastapi-permissioned-routes-example git:(main) ✗ 
$ curl -i -X 'GET' \
  'http://localhost:9999/v1/items' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -H  'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1c2VyQGV4YW1wbGUuY29tIiwiZXhwIjoxNjg3NDYyOTkyfQ.vjZL8kYk1wNk-w-bZ4Lzum6tuMtBI9-V6wpqKlIgwJs'

HTTP/1.1 200 OK
date: Thu, 22 Jun 2023 18:46:04 GMT
server: uvicorn
content-length: 55
content-type: application/json

[
    {
        "name":"item one",
        "id":1
    }
]
```

- USER 역할의 사용자로 로그인하고 항목을 삭제한다.

```python
➜  fastapi-permissioned-routes-example git:(main) ✗ 
$ curl -i -X 'DELETE' \
  'http://localhost:9999/v1/items/1' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -H  'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1c2VyQGV4YW1wbGUuY29tIiwiZXhwIjoxNjg3NDYyOTkyfQ.vjZL8kYk1wNk-w-bZ4Lzum6tuMtBI9-V6wpqKlIgwJs'

HTTP/1.1 403 Forbidden
date: Thu, 22 Jun 2023 18:47:01 GMT
server: uvicorn
content-length: 59
content-type: application/json

{
    "detail":"Not enough permissions to access this resource"
}
```

**다시 말하지만, 예상대로 USER 역할의 사용자에게는 이 리소스에 필요한 권한이 할당되지 않았으며, 이 경우 항목_삭제 따라서 권한이 거부된다!**

## 마치며

### 이점
이러한 방식으로 리소스 권한을 분석하면 다음과 같은 이점이 있다.

1. 어플리케이션에 역할을 추가하는 것은 사전을 수정하는 것만큼이나 간단하다.
1. 기존/새 리소스에 대한 권한을 추가/수정하는 것은 역할에 대한 권한 목록을 수정하는 것만으로 가능하다.
1. 서로 다른 리소스의 권한을 결합하여 역할을 원하는 만큼 복잡하게 만들 수 있고 리소스를 원하는 만큼 세밀하게 보호할 수 있다.

